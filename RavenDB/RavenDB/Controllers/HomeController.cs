﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Raven.Client;
using Raven.Client.Document;
using RavenDB.Models;

namespace RavenDB.Controllers
{
    public class HomeController : Controller
    {
        DocumentStore store = new DocumentStore {
           ConnectionStringName="Server"
        };
   
      
        public ActionResult Index(string id)
        {
            store.Initialize();
            //var person = new Person { Id="1",Name="Bill", Age=50};
            using (IDocumentSession session = store.OpenSession()) {
                var results = session.Load<Person>(id);
                              
                return View(results);
            }
            
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}